# Loop test

## Installation

- run `yarn`

## Development

```
$ yarn dev
```

Open **localhost:8080**

## Build

```
$ yarn build
```

## To run dist

```
$ npx serve -s dist
```
