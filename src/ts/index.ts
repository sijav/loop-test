import "core-js/modules/es.promise";
import "core-js/modules/es.array.iterator";
import "bootstrap";
import "../scss/index.scss";
import '../js/font-awesome.js';
import json from './team.json';

type MemberType = typeof json[0];

docReady(() => {
  main();
});

function main() {
  newsBilderTri();
  articleBottomTri();
  teamMember();
}

function newsBilderTri() {
  const canvas = document.getElementById('news-bilder') as HTMLCanvasElement;
  createNewsBilderTri(canvas);
}

function articleBottomTri() {
  const canvas = document.getElementById('article-bottom') as HTMLCanvasElement;
  createArticleBottomTri(canvas);
}

function teamMember() {
  let duty = 'all';
  let loading = false;
  const page = {};
  const datas: { [duty: string]: MemberType[] } = { all: [] };
  const teamMemberEl = document.getElementById('team-members') as HTMLDivElement;
  const menuEls = document.querySelectorAll('.menu ul li') as NodeListOf<HTMLLIElement>;
  const loadMoreEl = document.getElementById('load-more-button') as HTMLButtonElement;
  loadMoreEl.addEventListener('click', handleLoadMore);
  menuEls.forEach(el => el.addEventListener('click', onTeamMenuSelect));
  function onTeamMenuSelect(e: MouseEvent) {
    if (!loading) {
      const el = (e.target as HTMLLIElement);
      const curDuty = el.getAttribute('data-name');
      if(duty != curDuty) {
        duty = curDuty
        menuEls.forEach(el => el.classList.remove('active'));
        teamMemberEl.innerHTML = '';
        el.classList.add('active');
        handleGenerateMembers();
      }
    }
  }
  function handleLoadMore() {
    handleGenerateMembers(page[duty] + 1);
  }
  function handleGenerateMembers(curPage = 1) {
    if (!page[duty]) {
      page[duty] = 0;
    }
    if (page[duty] < curPage) {
      page[duty]++;
      loading = true;
      const loadingEl = appendLoading(teamMemberEl);
      getTeamMember(page[duty], duty).then(team => {
        teamMemberEl.removeChild(loadingEl);
        if(!datas[duty]) {
          datas[duty] = [];
        }
        datas[duty].push(...team);
        appendTeamMembers(team, teamMemberEl);
        loading = false;
      });
    } else {
      appendTeamMembers(datas[duty], teamMemberEl);
    }
  }
  handleGenerateMembers();
}

function docReady<T = null>(callback: (ctx?: T) => void, context?: T) {
  "use strict";
  // The public function name defaults to window.docReady
  // but you can modify the last line of this function to pass in a different object or method name
  // if you want to put them in a different namespace and those will be used instead of
  // window.docReady(...)
  var readyList = [];
  var readyFired = false;
  var readyEventHandlersInstalled = false;

  // call this when the document is ready
  // this function protects itself against being called more than once
  function ready() {
    if (!readyFired) {
      // this must be set to true before we start calling callbacks
      readyFired = true;
      for (var i = 0; i < readyList.length; i++) {
        // if a callback here happens to add new ready handlers,
        // the docReady() function will see that it already fired
        // and will schedule the callback to run right after
        // this event loop finishes so all handlers will still execute
        // in order and no new ones will be added to the readyList
        // while we are processing the list
        readyList[i].fn.call(window, readyList[i].ctx);
      }
      // allow any closures held by these functions to free
      readyList = [];
    }
  }

  function readyStateChange() {
    if (document.readyState === "complete") {
      ready();
    }
  }

  // This is the one public interface
  // docReady(fn, context);
  // the context argument is optional - if present, it will be passed
  // as an argument to the callback
  if (typeof callback !== "function") {
    throw new TypeError("callback for docReady(fn) must be a function");
  }
  // if ready has already fired, then just schedule the callback
  // to fire asynchronously, but right away
  if (readyFired) {
    setTimeout(function () { callback(context); }, 1);
    return;
  } else {
    // add the function and context to the list
    readyList.push({ fn: callback, ctx: context });
  }
  // if document already ready to go, schedule the ready function to run
  // IE only safe when readyState is "complete", others safe when readyState is "interactive"
  if (document.readyState === "complete" || (!(document as any).attachEvent && document.readyState === "interactive")) {
    setTimeout(ready, 1);
  } else if (!readyEventHandlersInstalled) {
    // otherwise if we don't have event handlers installed, install them
    if (document.addEventListener) {
      // first choice is DOMContentLoaded event
      document.addEventListener("DOMContentLoaded", ready, false);
      // backup is window load event
      window.addEventListener("load", ready, false);
    } else {
      // must be IE
      (document as any).attachEvent("onreadystatechange", readyStateChange);
      (window as any).attachEvent("onload", ready);
    }
    readyEventHandlersInstalled = true;
  }
}

function createNewsBilderTri(canvas: HTMLCanvasElement) {
  const currentWidth = canvas.width;
  const currentHeight = canvas.height;
  const context = canvas.getContext('2d');

  context.beginPath();

  context.moveTo(0, 0);
  context.lineTo(0, currentHeight);
  context.lineTo(currentWidth, 0);

  context.closePath();

  context.fillStyle = "rgb(0, 0, 0)";
  context.fill();
}

function createArticleBottomTri(canvas: HTMLCanvasElement) {
  const currentWidth = canvas.width;
  const context = canvas.getContext('2d');

  context.beginPath();

  context.moveTo(currentWidth, 0);
  context.lineTo(currentWidth, 160);
  context.lineTo(0, 160);

  context.closePath();

  context.fillStyle = "rgb(0, 0, 0)";
  context.fill();
}

function createTeamMember(user: MemberType) {
  const memberEl = document.createElement('div');
  memberEl.className = 'member';
  const imageEl = document.createElement('div');
  imageEl.className = 'image';
  imageEl.style.backgroundImage = `url(${user.image})`;
  memberEl.appendChild(imageEl);
  const infoEl = document.createElement('div');
  infoEl.className = 'info';
  const nameEl = document.createElement('h5');
  const nameTextEl = document.createTextNode(user.name);
  nameEl.appendChild(nameTextEl);
  infoEl.appendChild(nameEl);
  const dutyEl = document.createElement('h6');
  const dutyTextEl = document.createTextNode(user.duties);
  dutyEl.appendChild(dutyTextEl);
  infoEl.appendChild(dutyEl);
  memberEl.appendChild(infoEl);
  return memberEl;
}

function getTeamMember(page = 1, currentDuty: string): Promise<MemberType[]> {
  return new Promise(resolve => setTimeout(() => resolve(filterTeamMembers(json, currentDuty).slice((page - 1) * 15, page * 15)), 1000));
}

function filterTeamMembers(team: MemberType[], currentDuty: string) {
  return currentDuty !== 'all' ? team.filter(user => user.duty_slugs.find(v => v === currentDuty)) : team;
}

function appendLoading(container: HTMLDivElement) {
  const loadingEl = document.createElement('div');
  loadingEl.className = 'loading';
  const loadingTextEl = document.createTextNode('Loading');
  loadingEl.appendChild(loadingTextEl);
  const loadingIconEl = document.createElement('i');
  loadingIconEl.className = 'fas fa-sync-alt fa-spin ms-1';
  loadingEl.appendChild(loadingIconEl);
  container.append(loadingEl);
  return loadingEl;
}

function appendTeamMembers(team: MemberType[], container: HTMLDivElement) {
  team.forEach(user => container.appendChild(createTeamMember(user)));
}
